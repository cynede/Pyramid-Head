// LThread
// Dependency: none
#ifndef L_THREAD
#define L_THREAD
//------------------------------------------------------------------------------
#include "lili.h"
#include <pthread.h>
/**********************************************************************/
// LThread
/**********************************************************************/
//! Text threads management class.
/*!
  This class provides mechanisms to manage threads.
*/
class LThread {
public:
    //! Constructor.
    LThread(void * (*foo)(void*), void* args);
    //! Destructor.
    ~LThread();
    //! Executes the thread
    void execute();
protected:
    pthread_t thread;
    int iret;
    };
//------------------------------------------------------------------------------
#endif /* L_THREAD */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
