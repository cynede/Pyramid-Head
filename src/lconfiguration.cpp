/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "lconfiguration.h"

/**********************************************************************/
// LConfiguration
/**********************************************************************/
#ifdef L_CONFIGURATION
LConfiguration::LConfiguration() {
    }
//------------------------------------------------------------------------------
LConfiguration::~LConfiguration() {
    }
//------------------------------------------------------------------------------
int LConfiguration::parse(const LString& filename) {
    FILE* pFile;
    char line[1024];
    char token[1024];
    char value[1024];
    char* pch;
    /* open configuration file */
    pFile = fopen(filename, "r");
    if(pFile != NULL) {
        while(1) {
            /* fetch line from configuration file */
            if(fgets(line , 1024 , pFile) != NULL) {
                pch = strtok(line, " =\r\n");
                token[0] = 0;
                value[0] = 0;
                while(pch != NULL) {
                    if(strcmp(pch, "#") == 0)
                        break;

                    if(token[0] == 0) {
                        strncpy(token, pch, sizeof(token));
                        }
                    else {
                        strncpy(value, pch, sizeof(value));
                        break;
                        }
                    pch = strtok(NULL, " =\r\n");
                    }
                if(token[0] && value[0]) {
                    /* parse arguments here */
                    if(parseToken(LString(token), LString(value)) == L_ERROR) {
                        llog.log(LLog::error, LString("Unknown option in configuration file ") + LString(token) + " = " + LString(value));
                        return L_ERROR;
                        }
                    }
                }
            else {
                break;
                }
            }
        fclose(pFile);
        }
    else {
        llog.log(LLog::error, LString("Cannot open configuration file ") + filename);
        return L_ERROR;
        }
    return L_OK;
    }
#endif /* L_CONFOGURATION */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
