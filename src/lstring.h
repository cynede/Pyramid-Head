/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LString
// Dependency: L_BYTES
#ifndef L_STRING
#define L_STRING

#include "lili.h"
#include "lbytes.h"
/**********************************************************************/
// LString
/**********************************************************************/
class LBytes;
//! Text string management class.
/*!
  This class provides mechanisms to manage strings.
*/
class LString {
public:
    //! Default Constructor.
    /*!
      Create empty string.
    */
    LString();
    //! Constructor to copy a string
    /*!
      Copy an existing string
      \param str the string to be copied.
    */
    LString(const char* str);
    //! Constructor to copy a string from byte array
    /*!
      Copy an existing string
      \param bytes the string to be copied.
      \param offset start copy from offset byte.
    */
    LString(const LBytes& bytes, int offset = 0);
    //! Copy constructor
    /*!
      Copy an existing string
      \param array string to be copied.
    */
    LString(const LString& other);
    //! Destructor.
    /*!
      This frees the string from memory.
    */
    ~LString();
    //! Clear the string.
    void clear();
    //! Get length of string .
    /*!
      Length is without terminating zero.
      \return the length of the string
    */
    int size() const;
    //! Check if string is empty.
    /*!
      Check if the string is empty.
      \return true if string is empty, else false
    */
    bool empty() const;
    //! Return pointer to string in memory.
    /*!
      \return pointer to string in memory
    */
    char* ptr() const;
    //! Append string.
    /*!
      \param other the string to be appended.
      \return pointer to itself
    */
    LString& append(const char* str);
    //! Append string.
    /*!
      \param other the string to be appended.
      \return pointer to itself
    */
    LString& append(const LString& other);
    //! Append operator.
    /*!
      \param other the string to be appended.
      \return new string object
    */
    LString operator+(const char* str);

    //! Append operator.
    /*!
      \param other the string to be appended.
      \return new string object
    */
    LString operator+(const LString& other);
    //! Copy operator.
    /*!
      \param str the string to be appended.
      \return pointer to itself
    */
    LString& operator+=(const char* str);
    //! Append operator.
    /*!
      \param other the string to be appended.
      \return pointer to itself
    */
    LString& operator+=(const LString& other);
    //! Copy string.
    /*!
      \param str the string to be copied.
      \return pointer to itself
    */
    LString& operator=(const char* str);
    //! Copy string.
    /*!
      \param other the string to be copied.
      \return pointer to itself
    */
    LString& operator=(const LString& other);
    //! Get pointer to string in memory.
    /*!
      \return pointer to string in memory
    */
    operator char* () const;
    //! Compare with other string.
    /*!
      \param str the string to be copied.
      \return true if they are equal, false if not
    */
    bool equal(const char* str) const;
    //! Compare with other string.
    /*!
      \param str the string to be copied.
      \return false if they are equal, true if not
    */
    bool operator!=(const char* str) const;
    //! Compare with other string.
    /*!
      \param str the string to be copied.
      \return true if they are equal, false if not
    */
    bool operator==(const char* str) const;
    //! Compare with other string.
    /*!
      \param other the string to be compared.
      \return false if they are equal, true if not
    */
    bool operator!=(const LString& other) const;
    //! Compare with other string.
    /*!
      \param other the string to be compared.
      \return true if they are equal, false if not
    */
    bool operator==(const LString& other) const;

    //! Find the position of a character in a string.
    /*!
      \param character the character to be searched.
      \param pos the search starts at the position pos in the string.
      \return the position of the first occurence of the character, -1 if the character was not found.
    */
    int find(const char character, int pos = 0) const;
    //! Find the position of a string in a string.
    /*!
      \param str the string to be searched.
      \param pos the search starts at the position pos in the string.
      \return the position of the first occurence of the string, -1 if the string was not found.
    */
    int find(const char* str, int pos = 0) const;
    //! Find the position of a string in a string.
    /*!
      \param other the string to be searched.
      \param pos the search starts at the position pos in the string.
      \return the position of the first occurence of the string, -1 if the string was not found.
    */
    int find(const LString& other, int pos = 0) const;
    //! Find the last position of a character in a string.
    /*!
      The search begins at the end of the string.
      \param character the character to be searched.
      \return the position of the last occurence of the character, -1 if the character was not found.
    */
    int findLastOf(const char character) const;
    //! Extract a sub string.
    /*!
      \param pos the extractions begins at the position pos in the string.
      \param length the number of characters to be extracted, -1 if the string until the end will be extracted.
      \return the substring, empty if paramaeters are out of range.
    */
    LString substr(int pos, int length = -1) const;
private:
    char* text;
    };
#endif /* L_STRING */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
