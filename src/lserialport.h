// LSerialPort
// Dependency: Llog
#ifndef L_SerialPort
#define L_SerialPort
//------------------------------------------------------------------------------
#include "llog.h"
#include <termios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
/**********************************************************************/
// LSerialPort
/**********************************************************************/
//! Text threads management class.
/*!
  This class provides mechanisms to manage serial port.
*/
class LSerialPort {
public:
    //! Default Constructor.
    /*!
      id is just id
     ,port name is something alike /dev/ttyS0
     ,baud is 0 - 5.
    */
    LSerialPort(int id, string portName, int baud);
    ~LSerialPort();
    int OpenPort();
    void ClosePort();
    void setrts(int on);
    int hCom, posBuf;
    char* devCom;
    char* buff_In;
//------------------------------------------------------------------------------
private:
    int idPort, baudRate;
    struct termios terminal;
    };
//------------------------------------------------------------------------------
#endif /* L_SerialPort */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
