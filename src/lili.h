/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef LILI_H
#define LILI_H

/**********************************************************************/
// Library configuaration
/**********************************************************************/
#define DEBUG           // -- DEBUG mode
#define WARN            // -- various WARN messages
#define TRICKS          // -- Tricky hacks

//#define gethostbyname   // -- use gethostbyname instead of getaddrinfo (obsolete)
/**********************************************************************/
// Includes needed by all
/**********************************************************************/
#include <iostream>
#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

using namespace std;
/**********************************************************************/
// Error values of LILI functions
/**********************************************************************/
//! Return values of LILI functions.
#define L_ERROR -1
#define L_OK     0
#define L_EXIT   1
#define L_END    2
//---------------------------------------------------------------------
#define LILI_VERSION "v0.0.1"

#endif /* LILI_H */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
