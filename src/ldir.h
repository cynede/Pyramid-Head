/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LDir
// Dependency: LString
#ifndef L_DIR
#define L_DIR
/**********************************************************************/
#include "lstring.h"
#include <dirent.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
/**********************************************************************/
// LDir
/**********************************************************************/
//!  Provides a class to manage and read directories.
/*!
  Get all files and subdirectory of a directory.
  Handle directory pathes.
  \todo Add also write events.
*/
class LDir {
public:
    //! This is the type of directory entry.
    typedef enum { File, Dir, Unknown } Type;
    //! Default constructor.
    LDir();
    //! Constructor which sets directly a path.
    /*!
      \param path the path to be set.
    */
    LDir(const LString& path);
    //! Constructor to copy an existing directory path.
    /*!
      \param dir Another directory object.
    */
    LDir(const LDir& dir);
    //! Destructor.
    ~LDir();
    //! Set the path of the directory.
    /*!
      \param _path the new directory path.
    */
    void setPath(LString _path);
    //! Get the current path.
    /*!
      \return the current set path.
    */
    LString getPath();
    //! Set the path to the current working directory.
    void setPathCwd();
    //! Operator to copy two directories pathes.
    /*!
      \param dir the directory path to be copied.
      \return pointer to itself.
    */
    LDir& operator=(const LDir& dir);
    //! This function selects the first directory entry of the set path.
    /*!
      \return L_ERROR = path invalid or no furtner directory entries, L_OK = first entry selected.
    */
    int first();
    //! This function selects the next directory entry of the set path.
    /*!
      \return L_ERROR = path invalid or no furtner directory entries, L_OK = next entry selected.
    */
    int next();
    //! Close the directory search.
    void close();
    //! Create a new directory with the name of the selected path.
    /*!
      \return L_ERROR or L_OK.
    */
    int createDirectory();
    //! Remove file selected by path.
    /*!
      The path includes also the filename.
      \return L_ERROR or L_OK.
    */
    int removeFilename();
    //! This function tries to find a subdirectory in all higher level directories.
    /*!
      It searches form low level to higher level directories.
      \param dirname the sub directory to be searched for.
      \return the name of the directory including the directory name dirname.
    */
    LString findSubdir(LString dirname);
    //! Checks if the selected directory path exists.
    /*!
      \return true = exists, false = not exists.
    */
    bool exist();
    //! This returns the filename of the current selected filepath.
    /*!
      \return the filename.
    */
    LString getFilename();
    //! This returns the type of the current selected filepath.
    /*!
      Possible types are directory or file.
      \see Type
      \return the filename.
    */
    Type getType();
protected:
private:
    LString path;
    DIR* dp;
    LString filename;
    Type type;
    };
#endif /* L_DIR */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
