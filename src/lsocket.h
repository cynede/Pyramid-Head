/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LSocket
// Dependency: LString
#ifndef L_SOCKET
#define L_SOCKET

#include "lstring.h"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/param.h>
#include <netdb.h>
/**********************************************************************/
// LSocket
/**********************************************************************/
//!  Handle TCP/IP Socket connections for server and clients.
/*!
  Manage socket conntecions and recv and send data.
*/
class LSocket {
public:
    //! Default constructor.
    LSocket();
    //! Constructor with existing handle.
    /*!
      \param handle handle to be used.
    */
    LSocket(int handle);
    //! Constructor which copies existing socket.
    /*!
      \param socket socket to be copied.
    */
    LSocket(const LSocket& socket);
    //! Destructor
    ~LSocket();
    //! Initialiase socket as server.
    /*!
      \param port The port the server waits for incoming connections.
      \return L_ERROR or L_OK.
    */
    int initServer(int port);
    //! Initialise socket as client.
    /*!
      \return L_ERROR or L_OK.
    */
    int initClient();
    //! Client connects to a server.
    /*!
      \param address the directory path to be copied.
      \param address the directory path to be copied.
      \return L_ERROR or L_OK.
    */
    int connect(const LString& address, int port);
    //! Accept connection of a listening server.
    /*!
      \return The new socket object contains the new accepted connection.
    */
    LSocket* accept();
    //! Recv data from a socket
    /*!
      \param data contains the received data after function call.
      \return L_ERROR or L_OK.
    */
    int recv(LBytes& data);
    //! Send some bytes to the opened socket.
    /*!
      \param data the data to be sent.
      \return L_ERROR or L_OK.
    */
    int send(const LBytes& data);
    //! Close the socket connection.
    void close();
    //! Get the file descriptor of the current socket connection.
    /*!
      \return file descriptor.
    */
    int getHandle();
protected:
private:
    int handle;
    };
#endif /* L_SOCKET */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
