/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "lsocket.h"

/**********************************************************************/
// LSocket
/**********************************************************************/
#ifdef L_SOCKET
LSocket::LSocket() {
    handle = 0;
    }
//------------------------------------------------------------------------------
LSocket::LSocket(int _handle) {
    handle = _handle;
    }
//------------------------------------------------------------------------------
LSocket::LSocket(const LSocket& socket) {
    handle = socket.handle;
    }
//------------------------------------------------------------------------------
LSocket::~LSocket() {
    if(handle > 0)
        ::close(handle);
    }
//------------------------------------------------------------------------------
int LSocket::initServer(int port) {
    int yes = 1;
    struct sockaddr_in servAddr;
    if((handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        return L_ERROR;
    setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family      = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port        = htons(port);
    if(bind(handle, (struct sockaddr*) &servAddr, sizeof(servAddr)) < 0)
        return L_ERROR;
    if(listen(handle, 3) < 0)
        return L_ERROR;
    return L_OK;
    }
//------------------------------------------------------------------------------
int LSocket::initClient() {
    return L_OK;
    }
//------------------------------------------------------------------------------
#ifdef gethostbyname
int LSocket::connect(const LString& address, int port) {
    struct sockaddr_in servAddr;
    struct hostent* host;    // Structure containing host information
    // open socket
    if((handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        return L_ERROR;
    if((host = (struct hostent*) gethostbyname(address)) == 0)
        return L_ERROR;
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family      = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr*)(host -> h_addr_list[0])));
    servAddr.sin_port        = htons(port);
    if(::connect(handle, (struct sockaddr*) &servAddr, sizeof(servAddr)) < 0)
        return L_ERROR;
    return L_OK;
    }
#else
int LSocket::connect(const LString& address, int port) {
    struct sockaddr_in servAddr;
    /* open socket */
    if((handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        return L_ERROR;
    //gethostbyname by getaddrinfo replacement
    addrinfo hints = {sizeof(addrinfo)};
    hints.ai_flags = AI_ALL;
    hints.ai_family = PF_INET;
    hints.ai_protocol = 4; //IPPROTO_IPV4
    addrinfo* pResult = NULL;
    int errcode = getaddrinfo(address, NULL, &hints, &pResult);
    if(errcode != 0)
        return L_ERROR;
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family      = AF_INET;
    servAddr.sin_addr.s_addr = *((uint32_t*) & (((sockaddr_in*)pResult->ai_addr)->sin_addr));
    servAddr.sin_port        = htons(port);
    if(::connect(handle, (struct sockaddr*) &servAddr, sizeof(servAddr)) < 0)
        return L_ERROR;
    return L_OK;
    }
#endif
//------------------------------------------------------------------------------
LSocket* LSocket::accept() {
    socklen_t cli_size;
    struct sockaddr cli;
    int in_sock;
    /* event from TCP server socket, new connection */
    cli_size = sizeof(cli);
    try {
        if((in_sock  = ::accept(handle, &cli, &cli_size)) < 0) {
            throw errno;
            return NULL;
            }
        else {
            return new LSocket(in_sock);
            }
        }
    catch(int error) {
        cout << "Error code " << error << " (" << strerror(error) << ")\n";
        }
    }
//------------------------------------------------------------------------------
int LSocket::recv(LBytes& data) {
    unsigned char buf[1024];
    int bytes;
    bytes = ::recv(handle, buf, 1024, 0);
    if(bytes <= 0)
        return L_ERROR;
    data.setData(buf, bytes);
    return L_OK;
    }
//------------------------------------------------------------------------------
int LSocket::send(const LBytes& data) {
    int bytes;
    bytes = ::send(handle, data.ptr(), data.size(), 0);
    if(bytes < 0)
        return L_ERROR;
    return L_OK;
    }
//------------------------------------------------------------------------------
void LSocket::close() {
    }
//------------------------------------------------------------------------------
int LSocket::getHandle() {
    return handle;
    }
//------------------------------------------------------------------------------
#endif /* L_SOCKET */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
