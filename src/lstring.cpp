/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//For GCC 4.6.2 +
//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wreturn-type"
//pragma GCC diagnostic pop

//For other GCC (for now, I'm using this)
//add -Wno-return-type

#include <lstring.h>

/**********************************************************************/
// LString
/**********************************************************************/
#ifdef L_STRING
LString::LString() {
    text = new char[1];
    text[0] = 0;
    }
//------------------------------------------------------------------------------
LString::LString(const char* str) {
    text = new char[strlen(str) + 1];
    strcpy(text, str);
    }
//------------------------------------------------------------------------------
LString::LString(const LBytes& bytes, int offset) {
    if(offset >= bytes.size()) {
        // error empty string
        text = new char[1];
        text[0] = 0;
        }
    else {
        text = new char[bytes.size() - offset + 1];
        strncpy(text, (char*)bytes.ptr() + offset, bytes.size() - offset);
        text[bytes.size() - offset] = 0;
        }
    }
//------------------------------------------------------------------------------
LString::LString(const LString& other) {
    text = new char[strlen(other.text) + 1];
    strcpy(text, other.text);
    }
//------------------------------------------------------------------------------
LString::~LString() {
    free(text);
    }
//------------------------------------------------------------------------------
void LString::clear() {
    free(text);
    text = new char[1];
    text[0] = 0;
    }
//------------------------------------------------------------------------------
int LString::size() const {
    return strlen(text);
    }
//------------------------------------------------------------------------------
bool LString::empty() const {
    return (strlen(text) == 0);
    }
//------------------------------------------------------------------------------
char* LString::ptr() const {
    return text;
    }
//------------------------------------------------------------------------------
LString::operator char* () const {
    return text;
    }
//------------------------------------------------------------------------------
LString& LString::append(const char* str) {
    char* ptr;
    ptr = new char[strlen(str) + strlen(text) + 2];
    strcpy(ptr, text);
    strcat(ptr, str);
    delete text;
    text = ptr;
    return *this;
    }
//------------------------------------------------------------------------------
LString& LString::append(const LString& other) {
    return append(other.text);
    }
//------------------------------------------------------------------------------
LString&  LString::operator+=(const char* str) {
    return append(str);
    }
//------------------------------------------------------------------------------
LString& LString::operator+=(const LString& other) {
    return append(other);
    }
//------------------------------------------------------------------------------
LString LString::operator+(const char* str) {
    return LString(*this).append(str);
    }
//------------------------------------------------------------------------------
LString LString::operator+(const LString& other) {
    return LString(*this).append(other);
    }
//------------------------------------------------------------------------------
//Ignoring warning
LString&  LString::operator=(const char* str) {
    text = new char[strlen(str) + 1];
    strcpy(text, str);
    }
//------------------------------------------------------------------------------
//Ignoring warning
LString&  LString::operator=(const LString& other) {
    *this = other.text;
    }
//------------------------------------------------------------------------------
bool LString::equal(const char* str) const {
    return (strcmp(text, str) == 0);
    }
//------------------------------------------------------------------------------
bool LString::operator!=(const LString& other) const {
    return !equal(other.text);
    }
//------------------------------------------------------------------------------
bool LString::operator==(const LString& other) const {
    return equal(other.text);
    }
//------------------------------------------------------------------------------
bool LString::operator!=(const char* str) const {
    return !equal(str);
    }
//------------------------------------------------------------------------------
bool LString::operator==(const char* str) const {
    return equal(str);
    }
//------------------------------------------------------------------------------
int LString::find(const char character, int pos) const {
    if(pos < 0 || pos >= (int)strlen(text))
        return -1;
    char* ptr = strchr(text + pos, character);
    if(!ptr)
        return -1;
    return (ptr - text);
    }
//------------------------------------------------------------------------------
int LString::find(const char* str, int pos) const {
    if(pos < 0 || pos >= (int)strlen(text))
        return -1;
    char* ptr = strstr(text + pos, str);
    if(!ptr)
        return -1;
    return (ptr - text);
    }
//------------------------------------------------------------------------------
int LString::find(const LString& other, int pos) const {
    return find(other.text, pos);
    }
//------------------------------------------------------------------------------
int LString::findLastOf(const char character) const {
    for(int num = strlen(text); num >= 0; num--) {
        if(text[num] == character)
            return num;
        }
    return -1;
    }
//------------------------------------------------------------------------------
LString LString::substr(int pos, int length) const {
    LString dest;
    if(pos < 0 || pos >= (int)strlen(text))
        return dest;
    delete dest.text;
    if(length == -1) {
        dest.text = new char[strlen(text) - pos];
        strncpy(dest.text, text + pos, strlen(text) - pos);
        dest.text[strlen(text) - pos] = 0 ;
        }
    else {
        dest.text = new char[length + 1];
        strncpy(dest.text, text + pos, length);
        dest.text[length] = 0;
        }
    return dest;
    }
//------------------------------------------------------------------------------
#endif /* L_STRING */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
