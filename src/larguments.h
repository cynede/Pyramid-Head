/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LArguments
// Dependency: L_LOG, L_TIME
#ifndef L_ARGUMENTS
#define L_ARGUMENTS

#include "llog.h"
#include "ltime.h"

/**********************************************************************/
// LArguments
/**********************************************************************/
//!  Parsing of an existing ini style configuration file.
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  */
class LArguments {
public:
    //! The constructor.
    /*!
    */
    LArguments();
    //! The destructor.
    /*!
    */
    ~LArguments();
    //! This function parses a complete argument list.
    /*!
      The list is provided by main function.
      \param argc the number of arguments.
      \param argv the arguments.
      \return L_ERROR or L_OK.
    */
    int parse(int argc, char* argv[]);
    //! This virtual function is called when an option as argument is detected.
    /*!
      Options are something like "-v" or "--version"
      \param option the name of the option.
      \return L_ERROR or L_OK.
    */
    virtual int parseOption(LString& option) = 0;
    //! This virtual function is called when an option with parameter as argument is detected.
    /*!
      Options are something like "-value=10" or "--value=10"
      \param option the name of the option.
      \param parameter the value of the optionn.
      \return L_ERROR or L_OK.
    */
    virtual int parseOptionWithParamater(LString& option, LString& parameter) = 0;
    //! This virtual function is called when a command is detected.
    /*!
      Commands are arguments without starting with - or --
      \param argument the command.
      \return L_ERROR or L_OK.
    */
    virtual int parseArgument(LString& argument) = 0;
    //! Virtual functions which should display the version information.
    /*!
      Is called when the arguments "-v" or "--version" are detected.
    */
    virtual void printVersion() = 0;
    //! Virtual functions which should display the usage information.
    /*!
      Is called when the arguments are wrong or contains no commands.
    */
    virtual void printUsage() = 0;
protected:
private:
    //! Internal function.
    int parseInternalOption(LString option);
    //! Internal function.
    int parseInternalOptionWithParamater(LString option, LString parameter);
    //! Internal function.
    int parseInternalArgument(LString argument);
    };
#endif /* L_ARGUMENTS */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
