/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LConfiguration
// Dependency: L_LOG, L_TIME
#ifndef L_CONFIGURATION
#define L_CONFIGURATION

#include "llog.h"
#include "ltime.h"

/**********************************************************************/
// LConfiguration
/**********************************************************************/
//!  Parsing of an existing ini style configuration file.
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  */
class LConfiguration {
public:
    //! The constructor.
    /*!
    */
    LConfiguration();
    //! The destructor.
    /*!
    */
    ~LConfiguration();
    //! This function parses a complete .
    /*!
      \param filename the filename of the configuration file to be parsed.
      \return L_ERROR or L_OK.
    */
    int parse(const LString& filename);
    //! This virtual function is called whenever a configuration value is detected.
    /*!
      \param token the name of the configuration parameter
      \param value the value of the configuration parameter.
      \return L_ERROR or L_OK.
    */
    virtual int parseToken(LString token, LString value) = 0;
protected:
private:
    LString filename;
    };
#endif /* L_CONFIGURATION */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
