// LDatabase
// Dependence: LString
#ifndef L_DATABASE
#define L_DATABASE
//------------------------------------------------------------------------------
#include "lstring.h"
#include <vector>
#include <sqlite3.h>
//------------------------------------------------------------------------------
//! Text Sqlite3 management class.
/*!
  This class provides mechanisms to manage Sqlite3 database.
*/
class LDatabase {
public:
    LDatabase(LString filename);
    ~LDatabase();
    bool open(LString filename);
    bool open();
    bool execute(LString query);
    vector<vector<LString> > query(LString query);
    void close();
private:
    static int callback(void *NotUsed, int argc, char **argv, char **szColName);
    sqlite3* database;
    char *szErrMsg;
    char* database_name;
    };
//------------------------------------------------------------------------------
#endif /* L_DATABASE */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
