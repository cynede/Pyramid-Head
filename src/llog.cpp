/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "llog.h"

/**********************************************************************/
// LLog
/**********************************************************************/
#ifdef L_LOG
LLog llog;
//------------------------------------------------------------------------------
LLog::LLog() {
    loglevel = LLog::info;
    }
//------------------------------------------------------------------------------
LLog::~LLog() {
    }
//------------------------------------------------------------------------------
void LLog::log(Level level, LString text) {
    /* do not show logs which are higher then the current log level */
    if(level > loglevel)
        return;
    /* output current time */
    cout << LTime().toString("%Y/%m/%d %H:%M:%S ");
    /* output log level */
    switch(level) {
        case LLog::fatal:
            cout << "fatal";
            break;
        case LLog::error:
            cout << "error";
            break;
        case LLog::warn:
            cout << "warn";
            break;
        case LLog::info:
            cout << "info";
            break;
        case LLog::debug:
            cout << "debug";
            break;
        case LLog::verbose:
            cout << "verbose";
            break;
        }
    /* output log message */
    cout << ": " << text << endl;
    }
//------------------------------------------------------------------------------
void LLog::setLogLevel(Level _loglevel) {
    loglevel = _loglevel;
    }
//------------------------------------------------------------------------------
LLog::Level LLog::getLogLevel() {
    return loglevel;
    }
//------------------------------------------------------------------------------
#endif /* L_LOG */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
