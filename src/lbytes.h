/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LBytes
// Dependency: L_STRING
#ifndef L_BYTES
#define L_BYTES

#include "lili.h"
#include "lstring.h"
/**********************************************************************/
// LBytes
/**********************************************************************/
class LString;
//!  Manages byte arrays in memory.
/*!
  This class provides mechanisms to manage byte arrays in RAM.
*/
class LBytes {
public:
    //! DefaultConstructor.
    /*!
      Create empty byte array.
    */
    LBytes();
    //! Copy constructor
    /*!
      Copy existing byte array
      \param array bytes to be copied.
    */
    LBytes(const LBytes& array);
    //! Constructor to fill memory object.
    /*!
      \param data the pattern with which the byte array is filled
      \param length the number of bytes to be generated.
    */
    LBytes(unsigned char data, int length);
    //! Constructor to copy bytes from memory.
    /*!
      \param data the pointer to the memory.
      \param length the number of bytes to be copied.
    */
    LBytes(const unsigned char* data, int length);
    //! Destructor.
    /*!
      This frees the reserved memory.
    */
    ~LBytes();
    //! Returns the number of bytes in the byte array.
    /*!
      \return the number of bytes in object.
    */
    int size() const;
    //! Returns if byte array contains some data.
    /*!
      \return true = empty, false = contains some data.
    */
    bool empty() const;
    //! Removes all data form byte array.
    void clear();
    //! This returns the pointer of the byte array in memory
    /*!
      \return pointer to the byte array in memory.
    */
    unsigned char* ptr() const;
    //! extract data from offset
    /*!
      \param offset extract offset.
      \param _length the target size of the byte array.
    */
    LBytes extract(int offset, int _length = 0);
    //! Change the size of the byte array.
    /*!
      If the current size is bigger, data is removed from the end.
      If the current size is smaller, new bytes are added but not initialised.
      \param length the target size of the byte array.
    */
    void resize(int length);
    //! Create a byte array with randome data.
    /*!
      \param length the target size of the byte array.
    */
    void randomise(int length);
    //! Copy data from another byte array.
    /*!
      \param array the byte array to be copied.
    */
    void setData(const LBytes& array);
    //! Create byte array with a fixed pattern
    /*!
      \param data the pattern with which byte array is filled.
      \param length the length of the new byte array.
    */
    void setData(unsigned char data, int length);
    //! Copy data from memory.
    /*!
      \param data pointer to the memory.
      \param length The new length of the byte array.
    */
    void setData(const unsigned char* data, int length);
    //! Copy operator.
    /*!
      \param array the bytes to be copied.
      \return pointer to itself
    */
    LBytes& operator = (const LBytes& array);
    //! Compare operator.
    /*!
      \param array the bytes to be compared.
      \return true if equal, else false
    */
    bool operator == (const LBytes& array);
    //! Compare operator.
    /*!
      \param array the bytes to be compared.
      \return true if equal, else false
    */
    bool operator != (const LBytes& array);
    //! Operator to append data from another byte array.
    /*!
      \param array the byte array to be appended.
      \return pointer to itself
    */
    LBytes& operator += (const LBytes& array);
    //! Append data from another byte array.
    /*!
      \param array the byte array to be appended.
    */
    void append(const LBytes& array);
    //! Returns the byte array as string in hex.
    /*!
      \return the string in hex.
    */
    LString toString();
protected:
private:
    unsigned char* data;
    int length;
    };
#endif /* L_BYTES */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
