/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LLog
// Dependency: L_TIME
#ifndef L_LOG
#define L_LOG

#include "ltime.h"
/**********************************************************************/
// LLog
/**********************************************************************/
//!  Logging information and errors of other L classes or applications using L.
/*!
  This class provides a logging facility, to log information or errors of of operation.
  The current implementation provides only logging to the console output.

  The log level up to log messages are shown can be set.
    Different log levels can be used for each log message.

  A global instance with the name lililog is created, which is used by all L classes.
  This instance can also be used by applications.

  \todo Provide logging interface to file.
  \todo Provide logging interface to syslog.
  \todo Provide logging interface to DLT.
*/
class LLog {
public:
    //! The constructor.
    /*!
    */
    LLog();
    //! The destructor.
    /*!
    */
    ~LLog();
    //! The different log levels of a log message.
    /*!
    */
    typedef enum { fatal, error, warn, info, debug, verbose } Level;
    //! Writing a log message into the log.
    /*!
      \param level the level of the log level. See Level.
      \param text the log level itself.
      \sa Level
    */
    void log(Level level, LString text);
    //! Set the log level, until logs are shown.
    /*!
      \param loglevel log message up to this level are shown. See Level.
      \sa Level
    */
    void setLogLevel(Level loglevel);
    //! Get the log level, until logs are shown.
    /*!
      \return up to this log level messages are shown.
      \sa Level
    */
    Level getLogLevel();
protected:
private:
    Level loglevel;
    };
extern LLog llog;
#endif /* L_LOG */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
