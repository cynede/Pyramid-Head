#include "ldatabase.h"

//------------------------------------------------------------------------------
#ifdef L_DATABASE
//------------------------------------------------------------------------------
LDatabase::LDatabase(LString file) {
    database = NULL;
    szErrMsg = 0;
    open(file);
    }
//------------------------------------------------------------------------------
LDatabase::~LDatabase() { }
//------------------------------------------------------------------------------
bool LDatabase::open(LString file) {
    char* filename = new char[file.size() + 1];
    strcpy(filename, file.ptr());
    if(sqlite3_open(filename, &database) == SQLITE_OK) {
        database_name = filename;
        return true;
        }
    return false;
    }
//------------------------------------------------------------------------------
bool LDatabase::open() {
    if(sqlite3_open(database_name, &database) == SQLITE_OK)
        return true;
    return false;
    }
//------------------------------------------------------------------------------
int LDatabase::callback(void* NotUsed, int argc, char** argv, char** szColName) {
#ifdef DEBUG
    for(int i = 0; i < argc; i++) {
        cout << szColName[i] << " = " << argv[i] << std::endl;
        }
    cout << "\n";
#endif
    return 0;
    }
//------------------------------------------------------------------------------
bool LDatabase::execute(LString q) {
    int result = sqlite3_exec(database, q, callback, 0, &szErrMsg);
    if(result != SQLITE_OK) {
#ifdef DEBUG
        cout << "SQL Error: " << szErrMsg << std::endl;
#endif
        sqlite3_free(szErrMsg);
        return -1;
        }
    return result;
    }
//------------------------------------------------------------------------------
vector<vector<LString> > LDatabase::query(LString q) {
    sqlite3_stmt* statement;
    vector<vector<LString> > results;
    if(sqlite3_prepare_v2(database, q, -1, &statement, 0) == SQLITE_OK) {
        int cols = sqlite3_column_count(statement);
        int result = 0;
        while(true) {
            result = sqlite3_step(statement);
            if(result == SQLITE_ROW) {
                vector<LString> values;
                for(int col = 0; col < cols; col++) {
                    values.push_back((char*)
                                     sqlite3_column_text(statement, col));
                    }
                results.push_back(values);
                }
            else break;
            }
        sqlite3_finalize(statement);
        }
    string error = sqlite3_errmsg(database);
    if(error != "not an error")
        cout << "Error: " << q << " " << error << endl;
    return results;
    }
//------------------------------------------------------------------------------
void LDatabase::close() {
    sqlite3_close(database);
    }

#endif /* L_DATABASE */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
