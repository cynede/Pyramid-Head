/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef L_TIME
#define L_TIME

#include "lstring.h"
/**********************************************************************/
// LTime
/**********************************************************************/
//!  Manage the current time and provides time string.
/*!
  Get the current time in seconds.
  Get text string from time.
*/
class LTime {
public:
    //! Default constructor.
    /*!
      Sets the object to the current time.
    */
    LTime();
    //! Constructo sets time in seconds.
    /*!
      \param seconds the seconds to be set.
    */
    LTime(time_t seconds);
    //! Copy constructor.
    /*!
      \param tim the existent time object to be copied.
    */
    LTime(const LTime& tim);
    //! Destructor
    ~LTime();
    //! Copy operator.
    /*!
      \param tim copy another time object.
      \return pointer to itself.
    */
    LTime& operator=(const LTime& tim);
    //! Set the time to current time.
    /*!
      \return pointer to itself.
    */
    LTime&  current();
    //! Export time to string.
    /*!
      \param fmt the directory path to be copied.
      \return The returned string.
    */
    LString toString(const char* fmt = "%Y%m%d%H%M%S");
    //! Get the seconds of the time.
    /*!
      \return the seconds.
    */
    time_t getSeconds();
protected:
private:
    time_t seconds;
    };
#endif /* L_TIME */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
