/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "ltimer.h"
/**********************************************************************/
// LTimer
/**********************************************************************/
#ifdef L_TIMER
LTimer::LTimer() {
    clear();
    }
//------------------------------------------------------------------------------
LTimer::LTimer(const LTimer& timer) {
    *this = timer;
    }
//------------------------------------------------------------------------------
LTimer::LTimer(unsigned int mseconds) {
    start(mseconds);
    }
//------------------------------------------------------------------------------
LTimer::~LTimer() {
    }
//------------------------------------------------------------------------------
void LTimer::start(unsigned int mseconds) {
    msecondsStart = ticks();
    msecondsFinish = msecondsStart + mseconds;
    }
//------------------------------------------------------------------------------
void LTimer::clear() {
    msecondsStart = 0;
    msecondsFinish = 0;
    }
//------------------------------------------------------------------------------
bool LTimer::elapsed() {
    return (ticks() >= msecondsFinish);
    }
//------------------------------------------------------------------------------
unsigned int LTimer::togo() {
    return (msecondsFinish - ticks());
    }
//------------------------------------------------------------------------------
LTimer& LTimer::operator = (const LTimer& timer) {
    msecondsStart = timer.msecondsStart;
    msecondsFinish = timer.msecondsFinish;
    return *this;
    }
//------------------------------------------------------------------------------
unsigned int LTimer::ticks() {
    struct timespec ts;
    if(clock_gettime(CLOCK_MONOTONIC, &ts) == 0) {
        return (uint32_t)((((ts.tv_sec * 1000000) + (ts.tv_nsec / 1000))) / 1000); // in 1 ms = 1000 us
        }
    else {
        return 0;
        }
    }
//------------------------------------------------------------------------------
#endif /* L_TIMER */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
