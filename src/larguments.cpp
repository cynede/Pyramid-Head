/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "larguments.h"

/**********************************************************************/
// LArguments
/**********************************************************************/
#ifdef L_ARGUMENTS
LArguments::LArguments() {
    }
//------------------------------------------------------------------------------
LArguments::~LArguments() {
    }
//------------------------------------------------------------------------------
int LArguments::parse(int argc, char* argv[]) {
    LString opt;
    int find;
    int ret;
    for(int num = 1; num < argc; num++) {
        opt = LString(argv[num]);

        if(opt.find("--") == 0) {
            if((find = opt.find("=")) != (int)string::npos) {
                if((ret = parseInternalOptionWithParamater(opt.substr(0, find), opt.substr(find + 1))) != L_OK) {
                    return ret;
                    }
                }
            else {
                if((ret = parseInternalOption(opt)) != L_OK) {
                    return ret;
                    }
                }
            }
        else if(opt.find("-") == 0) {
            if((find = opt.find("=")) != (int)string::npos) {
                if((ret = parseInternalOptionWithParamater(opt.substr(0, find), opt.substr(find + 1))) != L_OK) {
                    return ret;
                    }
                }
            else {
                if((ret = parseInternalOption(opt)) != L_OK) {
                    return ret;
                    }
                }
            }
        else {
            if((ret = parseInternalArgument(opt)) != L_OK) {
                return ret;
                }
            }

        }
    return L_OK;
    }
//------------------------------------------------------------------------------
int LArguments::parseInternalArgument(LString argument) {
    int ret;
    ret = parseArgument(argument);
    if(ret < 0) {
        llog.log(LLog::error, LString("Unknown argument ") + argument);
        }
    return ret;
    }
//------------------------------------------------------------------------------
int LArguments::parseInternalOption(LString option) {
    int ret;
    if(option == "--version" || option == "-v") {
        printVersion();
        return L_EXIT;
        }
    if(option == "--help" || option == "-h") {
        printUsage();
        return L_EXIT;
        }
    ret = parseOption(option);
    if(ret < 0) {
        llog.log(LLog::error, LString("Unknown option ") + option);
        }
    return ret;
    }
//------------------------------------------------------------------------------
int LArguments::parseInternalOptionWithParamater(LString option, LString parameter) {
    int ret;
    ret = parseOptionWithParamater(option, parameter);
    if(ret < 0) {
        llog.log(LLog::error, LString("Unknown option ") + option + " = " + parameter);
        }
    return ret;
    }
//------------------------------------------------------------------------------
#endif /* L_ARGUMENTS */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
