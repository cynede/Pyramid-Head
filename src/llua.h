// LLUA
// Dependency: LString
#ifndef L_LUA
#define L_LUA
//------------------------------------------------------------------------------
#include "lstring.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "../contrib/lualib.h"
#include "../contrib/luaxlib.h"
#include "../contrib/lua.h"
#ifdef __cplusplus
}
#endif
/**********************************************************************/
// LLUA
/**********************************************************************/
//! Text threads management class.
/*!
  This class to use lua scripts in project
*/
class LLUA {
public:
    //! Default Constructor.
    LLUA();
    //! Destructor.
    ~LLUA();
    void executeScript();
    void checkTime(time_t new_time);
    LString script;
    LString name;
    int status;
    pthread_t thread;
protected:
    long long int id;
    time_t runTime;
    };
//------------------------------------------------------------------------------
#endif /* L_LUA */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
