/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "lselect.h"

/**********************************************************************/
// LSelect
/**********************************************************************/
#ifdef L_SELECT
LSelect::LSelect() {
    fdmax = 0;
    i = 0;
    }
//------------------------------------------------------------------------------
LSelect::~LSelect() {
    FD_ZERO(&(master));
    FD_ZERO(&(read_fds));
    }
//------------------------------------------------------------------------------
int LSelect::select(unsigned int mseconds) {
    struct timeval tv;
    /* Wait up to one second. */
    tv.tv_sec = 0;
    tv.tv_usec = mseconds * 1000;
    /* wait for     int findLastOf(const char character) const;
    events form all FIFO and sockets */
    read_fds = master;
    if(::select(fdmax + 1, &(read_fds), NULL, NULL, &tv) == -1)
        return L_ERROR;
    return L_OK;
    }
//------------------------------------------------------------------------------
void LSelect::addFd(int fd) {
    FD_SET(fd, &(master));
    if(fd > fdmax) {
        fdmax = fd;
        }
    }
//------------------------------------------------------------------------------
void LSelect::removeFd(int fd) {
    FD_CLR(fd, &(master));
    }
//------------------------------------------------------------------------------
int LSelect::first() {
    i = 0;
    return next();
    }
//------------------------------------------------------------------------------
int LSelect::next() {
    int selected;
    while(i <= fdmax) {
        if(FD_ISSET(i, &(read_fds))) {
            selected = i;
            i++;
            return selected;
            }
        i++;
        }
    return -1;
    }
//------------------------------------------------------------------------------
#endif /* L_SELECT */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
