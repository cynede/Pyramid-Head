// LTWI
// Dependency: LString
#ifndef L_TWI
#define L_TWI
//------------------------------------------------------------------------------
#include "lstring.h"
#include "llog.h"
#include "stdint.h"
#ifndef SUSE
#include <linux/i2c.h>
#endif
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>

#ifndef I2C_SLAVE
#define I2C_SLAVE       0x0703
#endif
/**********************************************************************/
// LTWI
/**********************************************************************/
//! Text threads management class.
/*!
  This class provides mechanisms to manage TWI / I2C interface.
*/
class LTWI {
public:
    //! Default Constructor.
    LTWI(LString _filename);
    //! Destructor.
    ~LTWI();
    int hCom;
protected:
    LString filename;
    char buff_In[1024];
    char buff_Out[1024];
    unsigned int posBuf;
    };
//------------------------------------------------------------------------------
#endif /* L_TWI */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
