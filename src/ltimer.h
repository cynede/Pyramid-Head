/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LTimer
// Dependency: none
#ifndef L_TIMER
#define L_TIMER

#include "lili.h"
#include "stdint.h"
#include "time.h"
/**********************************************************************/
// LTimer
/**********************************************************************/
//!  Timer object in milliseconds resolution
/*!
  Timer used to measure time in milliseconds.
*/
class LTimer {
public:
    //! Default constructor.
    /*!
      Timer is set to invalid.
    */
    LTimer();
    //! Copy constructor.
    /*!
      \param timer Timer to be copied.
    */
    LTimer(const LTimer& timer);
    //! Constructor provides milliseconds
    /*!
      Start timer.
      \param mseconds Milliseconds after which timer is elapsed.
    */
    LTimer(unsigned int mseconds);
    //! Destructor.
    ~LTimer();
    //! Start the timer.
    /*!
      \param mseconds The amount of milliseconds the timer runs.
    */
    void start(unsigned int mseconds);
    //! Clears and stops timer.
    void clear();
    //! Check if timer is still running.
    /*!
      \return true = time is elapsed, false = timer is still running.
    */
    bool elapsed();
    //! The number of milliseconds the timer is still running.
    /*!
      \return time in milliseconds the timer still runs.
    */
    unsigned int togo();
    //! Copy an existing timer.
    /*!
      \param timer The timer to be copied.
      \return pointer to itself.
    */
    LTimer& operator = (const LTimer& timer);
protected:
    //! Returns the current number of ticks the PC is running.
    /*!
      \return number of ticks.
    */
    unsigned int ticks();
private:
    unsigned int msecondsStart;
    unsigned int msecondsFinish;
    };
#endif /* L_TIMER */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
