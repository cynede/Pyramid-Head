/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <ltime.h>

/**********************************************************************/
// LTime
/**********************************************************************/
#ifdef L_TIME
LTime::LTime() {
    current();
    }
//------------------------------------------------------------------------------
LTime::LTime(time_t _seconds) {
    seconds = _seconds;
    }
//------------------------------------------------------------------------------
LTime::LTime(const LTime& tim) {
    *this = tim;
    }
//------------------------------------------------------------------------------
LTime::~LTime() {

    }
//------------------------------------------------------------------------------
LTime& LTime::operator=(const LTime& tim) {
    seconds = tim.seconds;
    }
//------------------------------------------------------------------------------
LTime&  LTime::current() {
    time(&seconds);
    }
//------------------------------------------------------------------------------
LString LTime::toString(const char* fmt) {
    struct tm* timeinfo;
    char buffer [80];
    timeinfo = localtime(&seconds);
    strftime(buffer, 80, fmt, timeinfo);
    return LString(buffer);
    }
//------------------------------------------------------------------------------
time_t LTime::getSeconds() {
    return seconds;
    }
//------------------------------------------------------------------------------
#endif /* L_TIME */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
