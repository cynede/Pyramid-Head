/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "ldir.h"

/**********************************************************************/
// LDir
/**********************************************************************/
#ifdef L_DIR
LDir::LDir() {
    dp = NULL;
    }
//------------------------------------------------------------------------------
LDir::LDir(const LString& _path) {
    path = _path;
    dp = NULL;
    }
//------------------------------------------------------------------------------
LDir::LDir(const LDir& dir) {
    *this = dir;
    dp = NULL;
    }
//------------------------------------------------------------------------------
LDir::~LDir() {
    close();
    }
//------------------------------------------------------------------------------
LDir& LDir::operator=(const LDir& dir) {
    path = dir.path;
    close();
    }
//------------------------------------------------------------------------------
int LDir::first() {
    /* close if last still open */
    close();
    if((dp  = opendir(path)) == NULL)
        return L_ERROR;
    return next();
    }
//------------------------------------------------------------------------------
int LDir::next() {
    struct dirent* dirp;
    struct stat sb;
    do {
        if((dirp = readdir(dp)) == NULL) {
            filename.clear();
            return L_ERROR;
            }
        filename = LString(dirp->d_name);
        }
    while(filename == "." || filename == "..");
    lstat((path + "/" + filename), &sb);
    if((sb.st_mode & S_IFDIR)) {
        type = LDir::Dir;
        }
    else if((sb.st_mode & S_IFREG)) {
        type = LDir::File;
        }
    else {
        type = LDir::Unknown;
        }
    return L_OK;
    }
//------------------------------------------------------------------------------
void LDir::close() {
    if(dp) closedir(dp);
    dp = NULL;
    }
//------------------------------------------------------------------------------
void LDir::setPathCwd() {
    char _path[MAXPATHLEN];
    if(getcwd(_path, MAXPATHLEN) != NULL) {
        path = LString(_path);
        }
    }
//------------------------------------------------------------------------------
int LDir::createDirectory() {
    struct stat sb;
    int pos = 1;
    int ret;
    pos = path.find('/', pos);
    while(pos != string::npos) {
        ret = lstat(path.substr(0, pos), &sb);
        if(ret) {
            if(mkdir(path.substr(0, pos), S_IRWXU) != 0)
                return L_ERROR;
            }
        pos = path.find('/', ++pos);
        }
    ret = lstat(path, &sb);
    if(ret || !(sb.st_mode & S_IFDIR)) {
        if(mkdir(path, S_IRWXU) != 0) {
            return L_ERROR;
            }
        }
    return L_OK;
    }
//------------------------------------------------------------------------------
int LDir::removeFilename() {
    int pos = 1;
    pos = path.findLastOf('/');
    if(pos != string::npos) {
        path = path.substr(0, pos);
        return L_OK;
        }
    return L_ERROR;
    }
//------------------------------------------------------------------------------
bool LDir::exist() {
    struct stat sb;
    int pos = 1;
    int ret;
    ret = lstat(path, &sb);
    if(ret) {
        return false;
        }
    if(sb.st_mode & S_IFDIR) {
        return true;
        }
    if(sb.st_mode & S_IFLNK) {
        return true;
        }
    return false;
    }
//------------------------------------------------------------------------------
LString LDir::findSubdir(LString dirname) {
    struct stat sb;
    int pos = 0;
    int ret;
    ret = lstat((path + "/" + dirname), &sb);
    if(ret == 0 && (sb.st_mode & S_IFDIR)) {
        return path;
        }
    pos = path.find('/', pos);
    while(pos != string::npos) {
        ret = lstat((path.substr(0, pos) + "/" + dirname), &sb);
        if(ret == 0 && (sb.st_mode & S_IFDIR)) {
            return path.substr(0, pos);
            }
        pos = path.find('/', ++pos);
        }
    return LString("");
    }
//------------------------------------------------------------------------------
void LDir::setPath(LString _path) {
    path = _path;
    }
//------------------------------------------------------------------------------
LString LDir::getPath() {
    return path;
    }
//------------------------------------------------------------------------------
LString LDir::getFilename() {
    return filename;
    }
//------------------------------------------------------------------------------
LDir::Type LDir::getType() {
    return type;
    }
//------------------------------------------------------------------------------
#endif /* L_DIR */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
