#include "llua.h"

/**********************************************************************/
// LLUA
/**********************************************************************/
#ifdef L_LUA
//------------------------------------------------------------------------------
LLUA::LLUA() {
    thread = 0;
    status = 0;
    }
//------------------------------------------------------------------------------
LLUA::~LLUA() {
    delete script;
    }
//------------------------------------------------------------------------------
void* threadScript(void* arg);
void LLUA::executeScript() {
    if(script == NULL || script.size() == 0)return;
    if(status != 0)return;
    int result;
    runTime = time(NULL);
    status = 1;
    result = pthread_create(&thread, NULL, threadScript, this);
    if(result == 0) {
        status = 0;
        return;
        }
    }
//------------------------------------------------------------------------------
void LLUA::checkTime(time_t new_time) {
    if(new_time - runTime > 3) {
        pthread_cancel(thread);
        thread = 0;
        status = 0;
        }
    }
//------------------------------------------------------------------------------
#endif /* L_LUA */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
