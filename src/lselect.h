/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

// LSelect
// Dependency: none
#ifndef L_SELECT
#define L_SELECT

#include "lili.h"

/**********************************************************************/
// LSelect
/**********************************************************************/
//!  Provides the select() mechanism.
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  \todo Add also write events.
*/
class LSelect {
public:
    //! The constructor.
    /*!
    */
    LSelect();
    //! The destructor.
    /*!
    */
    ~LSelect();
    //! This is the main function of the select mechanism.
    /*!
      This function call blocks until it receives at least one event.
      \param mseconds the time until the blocking call is stopped. 0 if it blocks forever.
      \return L_ERROR or L_OK.
    */
    int select(unsigned int mseconds);
    //! Add a file desktiptor to the select list.
    /*!
      \param fd the file descriptor to be added.
    */
    void addFd(int fd);
    //! Remove a file descriptor from the select list.
    /*!
      \param fd the file descriptor to be removed.
    */
    void removeFd(int fd);
    //! Get the first file descriptor, which received an event.
    /*!
      \return the first file descriptor, or -1 if no further file descriptor received an event.
    */
    int first();
    //! Get the next file descriptor, which received an event.
    /*!
      \return the next file decsriptor, or -1 if no further file descriptor received an event.
    */
    int next();
protected:
private:
    fd_set master;        /**< master set of handles */
    fd_set read_fds;      /**< read set of handles */
    int fdmax;            /**< highest number of used handles */
    int i;
    };
#endif /* L_SELECT */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
