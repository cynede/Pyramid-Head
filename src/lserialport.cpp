#include "lserialport.h"

/**********************************************************************/
/// LSerialPort
/**********************************************************************/
#ifdef L_SerialPort
//------------------------------------------------------------------------------
speed_t baud_rates[6] = {B4800, B9600, B19200, B38400, B57600, B115200};
//------------------------------------------------------------------------------
LSerialPort::LSerialPort(int id, string portName, int baud) {
    posBuf = 0;
    idPort = id;
    devCom = new char[portName.length() + 1];
    strcpy(devCom, portName.c_str());
    baudRate = baud;
    struct stat buf;
    /*int st =*/ stat(portName.c_str(), &buf);
    if (buf.st_uid == 0) {
        char* cmdc = new char[32];
        string usercmd = strcpy(cmdc, getenv("USER"));
        if (usercmd != "root") {
#ifdef TRICKS
#ifdef DEBUG
            llog.log(LLog::info, "trying to grant permission with sudo");
#endif
            char* cmdc = new char[32];
            string usercmd = strcpy(cmdc, getenv("USER"));
            usercmd = "sudo chown " + usercmd + "  ";
            char* outt = new char[portName.length() + usercmd.length() + 2];
            string outstr = usercmd + portName;
            strcpy(outt, outstr.c_str());
            if(system(NULL)) {
                if(system(outt)) {
                    llog.log(LLog::info, "done");
                    }
                }
            else {
                llog.log(LLog::error, "processor is not available");
                }
            delete [] cmdc;
            delete [] outt;
#else
        llog.log(LLog::error, "failed to open port, check file permissions");
#endif /*TRICKS*/
            }
        }
    if(OpenPort() == -1) {
        llog.log(LLog::error, "failed to open port");
        }
    }
//------------------------------------------------------------------------------
int LSerialPort::OpenPort() {
    struct termios options;
    /// open the device to be non-blocking (read will return immediatly)
    hCom = open(devCom, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if(hCom < 0) {
        perror(devCom);
        return(-1);
        }
#ifdef DEBUG
    llog.log(LLog::info, 
                LString(devCom) + " port opened!");
#endif /* DEBUG */
    ///set parameters of port
    tcgetattr(hCom, &options);
    ///disable soft flow control
    options.c_iflag = 0;
    options.c_oflag = 0;
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_cflag |= (CLOCAL | CREAD);
    ///8n1
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE; // Mask the character size bits
    options.c_cflag |= CS8;    // Select 8 data bits
    ///enable hard flow control
    options.c_cflag &= ~CRTSCTS;
    options.c_cc[VMIN]  = 10;
    options.c_cc[VTIME] = 10;

    cfsetispeed(&options, baud_rates[baudRate]);
    cfsetospeed(&options, baud_rates[baudRate]);

    tcsetattr(hCom, TCSANOW, &options);
    fcntl(hCom, F_SETFL, FNDELAY);
    return hCom;
    }
//------------------------------------------------------------------------------
void LSerialPort::ClosePort() {
    if(hCom > 0) {
        close(hCom);
        hCom = 0;
        }
    }
//------------------------------------------------------------------------------
LSerialPort::~LSerialPort() {
    ClosePort();
    }
//------------------------------------------------------------------------------
void LSerialPort::setrts(int on) {
    int controlbits;
    ioctl(hCom, TIOCMGET, &controlbits);
    if(on) {
        controlbits |= TIOCM_RTS;
        }
    else {
        controlbits &= ~TIOCM_RTS;
        }
    try {
        if(ioctl(hCom, TIOCMSET, &controlbits) < 0) {
            throw errno;
            }
        }
    catch(int error) {
        cout << "Serial port ioctl failed. Error code " << error << " (" << strerror(error) << ")\n";
        }
    }
//------------------------------------------------------------------------------
#endif /* L_SerialPort */

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
