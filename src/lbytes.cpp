/*
L - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <lbytes.h>

/**********************************************************************/
// LBytes
/**********************************************************************/
#ifdef L_BYTES
LBytes::LBytes() {
    data = (unsigned char*)malloc(0);
    length = 0;
    }
//------------------------------------------------------------------------------
LBytes::LBytes(const LBytes& array) {
    data = (unsigned char*)malloc(array.length);
    memcpy(data, array.data, array.length);
    length = array.length;
    }
//------------------------------------------------------------------------------
LBytes::LBytes(unsigned char _data, int _length) {
    data = (unsigned char*)malloc(_length);
    memset(data, _data, _length);
    length = _length;
    }
//------------------------------------------------------------------------------
LBytes::LBytes(const unsigned char* _data, int _length) {
    data = (unsigned char*)malloc(_length);
    memcpy(data, _data, _length);
    length = _length;
    }
//------------------------------------------------------------------------------
LBytes::~LBytes() {
    free(data);
    }
//------------------------------------------------------------------------------
int LBytes::size() const {
    return length;
    }
//------------------------------------------------------------------------------
unsigned char* LBytes::ptr() const {
    return data;
    }
//------------------------------------------------------------------------------
bool LBytes::empty() const {
    return length > 0 ? false : true;
    }
//------------------------------------------------------------------------------
void LBytes::clear() {
    free(data);
    data = (unsigned char*)malloc(0);
    length = 0;
    }
//------------------------------------------------------------------------------
LBytes LBytes::extract(int offset, int _length) {
    int newsize;
    if (_length == 0) {
        newsize = length - offset;
        }
    else {
        newsize = _length;
        }
    unsigned char* newMessage = new unsigned char[newsize];
    memcpy(&newMessage[0], &data[offset], newsize * sizeof(char) );
    LBytes newLbytes = LBytes(newMessage, newsize);
    delete[] newMessage;
    return newLbytes;
    }
//------------------------------------------------------------------------------
void LBytes::resize(int _length) {
    int oldlength = length;
    unsigned char* olddata = data;
    data = (unsigned char*)malloc(_length);
    memcpy(data, olddata, _length > oldlength ? oldlength : _length);
    free(olddata);
    length = _length;
    }
//------------------------------------------------------------------------------
void LBytes::randomise(int _length) {
    delete data;
    data = (unsigned char*)malloc(_length);
    length = _length;
    srand(time(NULL));
    for(int n = 0; n < length; n++) {
        data[n] = rand() % 256;
        }
    }
//------------------------------------------------------------------------------
void LBytes::setData(const LBytes& array) {
    free(data);
    data = (unsigned char*)malloc(array.length);
    memcpy(data, array.data, array.length);
    length = array.length;
    }
//------------------------------------------------------------------------------
void LBytes::setData(unsigned char _data, int _length) {
    free(data);
    data = (unsigned char*)malloc(_length);
    memset(data, _data, _length);
    length = _length;
    }
//------------------------------------------------------------------------------
void LBytes::setData(const unsigned char* _data, int _length) {
    free(data);
    data = (unsigned char*)malloc(_length);
    memcpy(data, _data, _length);
    length = _length;
    }
//------------------------------------------------------------------------------
LBytes& LBytes::operator = (const LBytes& array) {
    setData(array);
    return *this;
    }
//------------------------------------------------------------------------------
bool LBytes::operator == (const LBytes& array) {
    if(length != array.length)
        return false;
    if(length == 0)
        return true;
    return (memcmp(data, array.data, length) == 0);
    }
//------------------------------------------------------------------------------
bool LBytes::operator != (const LBytes& array) {
    return !(*this == array);
    }
//------------------------------------------------------------------------------
LBytes& LBytes::operator += (const LBytes& array) {
    append(array);
    return *this;
    }
//------------------------------------------------------------------------------
void LBytes::append(const LBytes& array) {
    int oldlength = length;
    unsigned char* olddata = data;
    data = (unsigned char*)malloc(oldlength + array.length);
    memcpy(data, olddata, oldlength);
    memcpy(data + oldlength, array.data, array.length);
    free(olddata);
    length = oldlength + array.length;
    }
//------------------------------------------------------------------------------
LString LBytes::toString() {
    char* str;
    LString output;
    if(length == 0) {
        return LString();
        }
    else {
        str = (char*)malloc(length * 2 + 1);
        for(int n = 0; n < length; n++) {
            sprintf(str + n * 2, "%02x", data[n]);
            }
        output.append(str);
        free(str);
        return output;
        }
    }
//------------------------------------------------------------------------------
#endif /* L_BYTES */
//------------------------------------------------------------------------------

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
