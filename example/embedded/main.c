// --------------------------------------------------------------------------*/
//         Headers
//------------------------------------------------------------------------------
#include <pio/pio_it.h>
#include <dbgu/dbgu.h>

#include <utility/trace.h>
#include <utility/assert.h>
#include <utility/math.h>
#include <utility/led.h>

#include <pmc/pmc.h>

#include <stdio.h>
#include <string.h>
//------------------------------------------------------------------------------
///         Local variables
//------------------------------------------------------------------------------
static const Pin leds[] = { // Debug
        {1 << 4, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT},
        {1 << 19, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}
    };
/// Indicates the current state (on or off) for each LED.
volatile unsigned char pLedStates[2] = {1, 1};
/// Pio pins to configure.
const Pin twipins[] = {PINS_DBGU, PINS_TWI};
//------------------------------------------------------------------------------
#if defined(AT91C_BASE_PITC)
//------------------------------------------------------------------------------
/// Handler for PIT interrupt. Increments the timestamp counter.
//------------------------------------------------------------------------------
void ISR_Pit(void) {
    unsigned int status;
    /// Read the PIT status register
    status = PIT_GetStatus() & AT91C_PITC_PITS;
    if(status != 0) {
        /// 1 = The Periodic Interval timer has reached PIV since the last read of PIT_PIVR.
        /// Read the PIVR to acknowledge interrupt and get number of ticks
        ///Returns the number of occurrences of periodic intervals since the last read of PIT_PIVR.
        timestamp += (PIT_GetPIVR() >> 20);
        }
    }
//------------------------------------------------------------------------------
/// Configure the periodic interval timer (PIT) to generate an interrupt every
/// millisecond.
//------------------------------------------------------------------------------
void ConfigurePit(void) {
    /// Initialize the PIT to the desired frequency
    PIT_Init(PIT_PERIOD, BOARD_MCK / 1000000);
    /// Configure interrupt on PIT
    IRQ_DisableIT(AT91C_ID_SYS);
    IRQ_ConfigureIT(AT91C_ID_SYS, AT91C_AIC_PRIOR_LOWEST, ISR_Pit);
    IRQ_EnableIT(AT91C_ID_SYS);
    PIT_EnableIT();
    /// Enable the pit
    PIT_Enable();
    }
#else
//------------------------------------------------------------------------------
/// Handler for Sytem Tick interrupt. Increments the timestamp counter.
//------------------------------------------------------------------------------
void SysTick_Handler(void) {
    timestamp++;
    }
#endif
//------------------------------------------------------------------------------
/// Configures the leds & Enable them.
//------------------------------------------------------------------------------
void ConfigureLeds(void) {
    // Configure pios
    PIO_Configure(&(leds[0]), 1);
    PIO_Configure(&(leds[1]), 1);
    PIO_Clear(&leds[0]);
    PIO_Clear(&leds[1]);
    }
//------------------------------------------------------------------------------
/// Interrupt handlers for TC interrupts. Toggles the state of LEDs
//------------------------------------------------------------------------------
char token = 0;
void TC0_IrqHandler(void) {
    volatile unsigned int dummy;
    dummy = AT91C_BASE_TC0->TC_SR;  
    if(token == 1) {
        PIO_Clear(&leds[0]);
        PIO_Set(&leds[1]);
        token = 0;
        }
    else {
        PIO_Set(&leds[0]);
        PIO_Clear(&leds[1]);
        token = 1;
        }
    }
//------------------------------------------------------------------------------
/// Configure Timer Counter 0 to generate an interrupt every 250ms.
//------------------------------------------------------------------------------
void ConfigureTc(void) {
    unsigned int div;
    unsigned int tcclks;
    AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_TC0;       // Enable peripheral clock
    TC_FindMckDivisor(1, BOARD_MCK, &div, &tcclks);     // Configure TC for a 4Hz frequency and trigger on RC compare
    TC_Configure(AT91C_BASE_TC0, tcclks | AT91C_TC_CPCTRG);
    AT91C_BASE_TC0->TC_RC = (BOARD_MCK / div) / 1;      // timerFreq / desiredFreq
    IRQ_ConfigureIT(AT91C_ID_TC0, 0, TC0_IrqHandler);   // Configure and enable interrupt on RC compare
    AT91C_BASE_TC0->TC_IER = AT91C_TC_CPCS;
    IRQ_EnableIT(AT91C_ID_TC0);
    printf(" -- timer has started \n\r");
    TC_Start(AT91C_BASE_TC0);
    }
//------------------------------------------------------------------------------
/// Default main() function. Initializes the DBGU and writes a string on the
//------------------------------------------------------------------------------
int main(void) {
    int* sygn;
    /// Configure the DBGU
    TRACE_CONFIGURE(DBGU_STANDARD, 115200, BOARD_MCK);
    DBGU_Configure(DBGU_STANDARD, 115200, BOARD_MCK);
    printf("-- SOFTPACK_VERSION %s --\n\r", SOFTPACK_VERSION);
    printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

    ///Twi PIO
    PIO_Configure(twipins, PIO_LISTSIZE(twipins));

    /// Enable TWI
    PMC_EnablePeripheral(AT91C_ID_TWI);

    /// Configure TWI as slave
    printf("-I- Configuring the TWI in slave mode\n\r");
    TWI_ConfigureSlave(AT91C_BASE_TWI, SLAVE_ADDRESS);
    
    /// Clear receipt buffer
    TWI_ReadByte(AT91C_BASE_TWI);
    printf("TWI is in slave mode\n\r");
    IRQ_ConfigureIT(AT91C_ID_TWI, 0, TWI0_IrqHandler);
    IRQ_EnableIT(AT91C_ID_TWI);
    TWI_EnableIt(AT91C_BASE_TWI, AT91C_TWI_SVACC);

    char c;
    DisplayMainmenu();
    
    while(1) {
        c = DBGU_GetChar();
        startADC(c);
        }
    
    return 0;
    }

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
