/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2009, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --------------------------------------------------------------------------*/
#include <adc.h>
//----------------------------------------------------------------------------
//         Local definition
//----------------------------------------------------------------------------
/// size of the receive buffer used by the PDC, in bytes.
#define BUFFER_SIZE         2000

//----------------------------------------------------------------------------
//         Local variables
//----------------------------------------------------------------------------

static const Pin Conf[] = {
    };

static const Pin ADCdata[] = {
    {1 << 16, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D0
    {1 << 17, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D1
    {1 << 18, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D2
    {1 << 19, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D3
    {1 << 20, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D4
    {1 << 21, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D5
    {1 << 22, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D6
    {1 << 23, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D7
    {1 << 24, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D8
    {1 << 25, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D9
    {1 << 26, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D10
    {1 << 27, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D11
    {1 << 28, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D12
    {1 << 29, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D13
    {1 << 30, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}, // D14
    {1 << 31, AT91C_BASE_PIOB, AT91C_ID_PIOB, PIO_OUTPUT_0, PIO_DEFAULT}  // D15
    };
    
static const Pin ADC[] = {
    {1 << 00, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // CS0
    {1 << 01, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // CS1
    {1 << 02, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // CS2
    {1 << 03, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // CS3
    {1 << 04, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // DRDY0
    {1 << 05, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // DRDY1
    {1 << 22, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // DRDY2
    {1 << 23, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // DRDY3
    {1 << 24, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // RW
    {1 << 25, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}, // Reset
    {1 << 26, AT91C_BASE_PIOA, AT91C_ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}  // SYNC
    };    
    
static const Pin DACpins[] = {
    {AT91C_PIO_PD0}, // LDAC0
    {AT91C_PIO_PD1}, // WR0
    {AT91C_PIO_PD2}  // RST0
    };

/// Transmit buffer.
char Buffer[BUFFER_SIZE] = //Test message
    " DESCRIPTION of this example: \n\r \
 **************************************************************************\n\r\
 This example demonstrates how to use USART in SPI mode. The USART is \n\r\
 configured as SPI master or slave. Meanwhile, the SPI peripheral in the \n\r\
 microcontroller is configured respectively, making it communicate with the \n\r \
 USART peripheral. The example leaves UART(DBGU) to let user to switch the command \n\r\
 between READ and WRITE from the view of master. \n\r\
 The application first initializes UART as the interface to interact with \n\r\
 users. By default, it initializes USART as SPI master and the SPI peripheral \n\r\
 as SPI slave. \n\r\
 The application waits for input from UART:\n\r\
      - If 'W' is received, the USART initiate a transmission by sending a \n\r\
      block of data. The SPI slave receives the data and dumps it to UART. \n\r\
      - If 'R' is received, the USART wait for data from SPI bus and dump it to\n\r\
      UART until the reception is finished. \n\r\
      - If 's' is received, the master and slave are exchanged between USART1\n\r\
      and SPI. To assure the transferring succeeds, connect pins as the table \n\r\
      above.\n\r\
 **************************************************************************\n\r\
 END of DESCRIPTION \n\r";
/// temporary buffer for master to generate clock.
char Buffer1[BUFFER_SIZE] = {0xaa, 0x55, 0x11, 0x22, 0x33};
volatile bool recvDone = false;
//------------------------------------------------------------------------------
/// Display main menu.
//------------------------------------------------------------------------------
void DisplayMainmenu() {
    printf(">-- Menu Choices for this example --\n\r");
    printf(">-- w: Write data block by master.--\n\r");
    printf(">-- r: Read data block by master.--\n\r");
    printf(">-- m: Display this menu again.--\n\r");
    }
//------------------------------------------------------------------------------
/// Dump buffer to UART(DBGU)
//------------------------------------------------------------------------------
void DumpInfo(char* buf, uint32_t size) {
    uint32_t i = 0;
    while((i < size) && (buf[i] != 0)) {
        printf("%c", buf[i++]);
        }
    }
//------------------------------------------------------------------------------
/// DAC WRITE
//------------------------------------------------------------------------------
void DACconfigure() {
    int i;
    for(i = 0; i < 3; i++) {
        PIO_Configure(&(DACpins[i]), i);
        }
    }
int DACwrite(char* data, int size) {
    jerk(DACpins[1], 1); // --------------- --------------------- WR
    jerk(DACpins[0], 1); // --------------- --------------------- LDAC
    jerk(DACpins[2], 1); // --------------- --------------------- RST
    return 0;
    }
//------------------------------------------------------------------------------
/// ADC READ
//------------------------------------------------------------------------------
void ADCconfigure() {
    int i;
    for(i = 0; i < 16; i++) {
        PIO_Configure(&(ADCdata[i]), i);
        }
    for(i = 0; i < 11; i++) {
        PIO_Configure(&(ADC[i]), i);
        }
    }

char data[256][16];
char* ADCread(int point) {    
    int i;
    bool switcher = false;
    bool isFirst = true;
    int j = 0;
    
    PIO_Clear(&ADC[8]); //RW
    for(i = 0; i < 4; i++) {
        PIO_Clear(&ADC[i]); // all CS to low
        }

    PIO_Clear(&ADC[9]);  // Reset to low
    PIO_Clear(&ADC[10]); // SYNC to low

    i = 4;
    recvDone = false;
    
    printf("ADC pulse\n\r");
    while(!recvDone) {
        if (switcher) {
            Wait(T_9);
            PIO_Set(&ADC[i]);
            Wait(T_11);
            if (isFirst) isFirst = false;
            else {
                memcpy(data[j], ADCdata, 16);
                j++;
                }
            switcher = false;
            }
        else {
            Wait(T_10);
            PIO_Clear(&ADC[i]);
            switcher = true;
            if (j > 255) {
                recvDone = true;
                }
            }
        };
    return (char*)data;
    }
//------------------------------------------------------------------------------
/// Application entry point.
/// \return Unused.
//------------------------------------------------------------------------------
void startADC(char c) {    
    switch(c) {
        case 'w':
        case 'W': {
            printf("DAC write test\n\r");
            if(DACwrite("Test", 5) != 0) {
                printf("DAC Error\n\r");
                }
            }   break;
        case 'r':
        case 'R': {
            printf("ADC read test\n\r");
            char* data;
            data = ADCread(0);
            }   break;
        case 'm':
        case 'M': {
            DisplayMainmenu();
            }   break;
        }
    }  

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
