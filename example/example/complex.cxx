#include <lili.h>
#include <lilu.h>
#include <lilith.h>
//------------------------------------------------------------------------------
#include <configuration.h>
#include <lexer.h>
//------------------------------------------------------------------------------
#include <libgen.h>     // for dirname
//------------------------------------------------------------------------------
#define DEBUG           // -- to see debug messages and run in terminal
#define WARN            // -- various WARN messages such as connections info
//------------------------------------------------------------------------------
LILITime ltime;
//------------------------------------------------------------------------------
void* select_server(void* ptr);
void* sync(void* ptr);
void* twi(void* ptr);
//------------------------------------------------------------------------------
char cCurrentPath[FILENAME_MAX], COMPort[32];
bool DEBUGMode;        // -- Debug mode for user.
//------------------------------------------------------------------------------
class DaemonArguments : public LILIArguments {
public:
    DaemonArguments() { }
    int parseOption(LILIString& option) {
        return LILI_ERROR;
        }
    int parseOptionWithParamater(LILIString& option, LILIString& parameter) {
        if(option == "-s" || option == "--source") {
            return LILI_OK;
            }
        else if(option == "-d" || option == "--destination") {
            return LILI_OK;
            }
        return LILI_ERROR;
        }
    int parseArgument(LILIString& argument) {
        if(argument == "status") {
            cmd = argument;
            return LILI_OK;
            }
        else if(argument == "start") {
            cmd = argument;
            return LILI_OK;
            }
        else if(argument == "stop") {
            cmd = argument;
            return LILI_OK;
            }
        return LILI_ERROR;
        }
    void printVersion() {
        cout << "complex version 0.0.1" << endl;
        }
    void printUsage() {
        cout << "usage: daemon start [stop]"            << endl;
        cout << "       [--version|-v] [--help|-h]"     << endl;
        cout << "       <command>"                      << endl;
        }
    LILIString cmd;
    };
//------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    DaemonArguments arguments;
    if(argc > 1) {
        if(arguments.parse(argc, argv) != LILI_OK) {
            return -1;
            }
        if(arguments.cmd != "start") {
            return 0;
            }
        }
    else {
        arguments.printUsage();
        return 0;
        }
#ifndef DEBUG
    pid_t pid, sid;   /* Our process ID and Session ID */
    pid = fork();     /*  Fork off the parent process  */
    /// If we got a good PID, then we can exit the parent process.
    if(pid < 0) {
        lililog.log(LILILog::error, "Forking from the main process failed");
        exit(EXIT_FAILURE);
        }
    if(pid > 0) exit(EXIT_SUCCESS);
    umask(0); /* Change the file mode mask */
    sid = setsid(); /* Create a new SID for the child process */
    if(sid < 0) {
        lililog.log(LILILog::error, "Creating a new SID for the child process failed");
        exit(EXIT_FAILURE);
        }
    if((chdir("/")) < 0) {  /* Change the current working directory */
        lililog.log(LILILog::error, "Changing the current working directory failed");
        exit(EXIT_FAILURE);
        }
#endif
#ifdef FILENO
    /// Close out the standard file descriptors
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
#endif
    /* Daemon-specific initialization goes here */
    ArmdConfiguration configuration;
    if(readlink("/proc/self/exe", cCurrentPath, FILENAME_MAX) != 0) {
        strcpy(cCurrentPath, dirname(cCurrentPath));
        }
    else {
        lililog.log(LILILog::error , "failed to get /proc/self/exe");
        }
    char Path[FILENAME_MAX];
    strcpy(Path, cCurrentPath);
    LILIString path = LILIString(strcat(Path, "/armd.conf"));
    if(configuration.parse(path) != LILI_OK) {
        strcpy(COMPort, "/dev/ttyS0");
        DEBUGMode = false;
        }
    else {
#ifdef WARN
        cout << "COMPort: " << configuration.COMPort << endl;
        cout << "DEBUGMode: " << configuration.DEBUGMode << endl;
        cout << "DatabasePath: " << configuration.DatabasePath << endl;
#endif
        strcpy(COMPort, configuration.COMPort.c_str());
        DEBUGMode = configuration.DEBUGMode;
        }

    ltime.current();
    /// Threadworks
    LILIThread* server_thread;
    LILIThread* twi_thread;
    LILIThread* sync_thread;

    LILIString* message1 = new LILIString("main");
    /// Daemon threads initialization
    server_thread = new LILIThread(select_server, message1);
    sync_thread = new LILIThread(sync, message1);
    twi_thread = new LILIThread(twi, message1);

    /// Parallel run the daemon threads
    server_thread->execute();

    exit(EXIT_SUCCESS);
    }
//------------------------------------------------------------------------------
void processdata(char* adc) {
    return;
    }
//------------------------------------------------------------------------------
void* twi(void* ptr) {
    char Path[FILENAME_MAX];
    char ADC[255];
    strcpy(Path, cCurrentPath);
    LILIString pathtwi = LILIString(strcat(Path, "/dev/twi"));
    LILITWI* twi = new LILITWI(pathtwi);
    
    twi->i2c_init();
    
    uint8_t data;
    uint8_t towrite;
    
    int i = 0;
    while(1) {
        data = twi->i2c_read();
        switch(data) {
            case 0x00: { // Get
                ADC[i] = data;
                if (i == 254) {
                    processdata(ADC);
                    i = 0;
                    }
                i++;
                towrite = 0x02;
                twi->i2c_write(towrite);
                }
                break;
            case 0x01: {
                towrite = 0x03;
                twi->i2c_write(towrite);
                }
                break;
            }
        }
    }
//------------------------------------------------------------------------------
void* sync(void* ptr) {
    char Path[FILENAME_MAX];
    strcpy(Path, cCurrentPath);
    LILIString pathsqlite = LILIString(strcat(Path, "/xdb.db3"));

    ///SQLite3
    LILIDatabase* db;
    db = new LILIDatabase(pathsqlite);
    while(1) {
#ifdef DEBUG
        lililog.log(LILILog::info, "armd is working");
        lililog.log(LILILog::info, ltime.toString());
#endif
        db->open();
#ifdef DEBUG
        ///Simple database poll
        vector<vector<string> > result = db->query("SELECT time_t,value FROM archive;");
        for(vector<vector<string> >::iterator it = result.begin(); it < result.end(); ++it) {
            vector<string> row = *it;
            cout
                    << "Values: (time_t=" << row.at(0)
                    << ", value=" << row.at(1)
                    << ")" << endl;
            }
#endif
        db->close();
        sleep(15);  /* 1 */
        }
    }
//------------------------------------------------------------------------------
void* select_server(void* ptr) {
    LILIBytes data;
    LILISocket* socket = 0;
    LILISocket server;

    server.initServer(2000);
#ifdef DEBUG
    lililog.log(LILILog::info, "Listening on port 2000");
#endif
//------------------------------------------------------------------------------
    LILISelect select;
    ///Ethernet handler
    select.addFd(server.getHandle());
    ///COMPort handlr
    LILISerialPort* port = new LILISerialPort(0, COMPort, 5);
    select.addFd(port->hCom);
//------------------------------------------------------------------------------
    Lexer* lexer = new Lexer();
    bool ports = false;
    while(1) {
        select.select(5000);
        int fd = select.first();
        if(fd == -1) {
#ifdef DEBUG
            lililog.log(LILILog::info, "Server timout after 5 sec");
#endif
            ports = false;
            }
//------------------------------------------------------------------------------
        while(fd != -1) {
            if(fd == server.getHandle()) {
                socket = server.accept();
                if  (socket != NULL) {
#ifdef WARN
                    lililog.log(LILILog::info, "Socket client connected");
#endif
                    select.addFd(socket->getHandle());
#ifdef DEBUG
                    lililog.log(LILILog::info, "got socket Handle");
#endif
                    }
                else {
                    lililog.log(LILILog::info, "Client connected but socket is null");
                    }
                }
            else if(socket && (fd == socket->getHandle())) {
                if(socket->recv(data) == LILI_ERROR) {
#ifdef WARN
                    lililog.log(LILILog::info, "Client disconnected");
#endif
                    select.removeFd(socket->getHandle());
                    socket->close();
                    delete socket;
                    socket = 0;
                    }
                else {
#ifdef DEBUG
                    cout << "Recived: " << LILIString(data) << endl;
#endif
                    LILIBytes lex = lexer->lex(data);
                    if(lex.size() > 0) {
                        switch(lex.ptr()[1]) {
                            case 0x00: {
                                socket->send(lex);
                                } break;
                            case 0x01: {
                                socket->send(lex);
                                } break;
                            }
                        }
                    }
                }
            if(fd == port->hCom) {
                if(!ports) {
#ifdef DEBUG
                    cout      << port->devCom
                              << " port is alive"
                              << endl;
#endif
                    ports = true;
                    }
                else {
                    int res = port->posBuf;
                    port->buff_In = new char[256];
                    res = read
                          (port->hCom
                           , &port->buff_In[port->posBuf]
                           , 256 - port->posBuf); //1024
                    if(res <= 0) {
                        lililog.log(LILILog::error, "Error reading pos");
                        ports = false;
                        }
                    else {
#ifdef DEBUG
                        cout << "Recived: " << port->buff_In << endl;
#endif
                        LILIBytes lex = lexer->lex(LILIBytes((unsigned char*)port->buff_In, res));
                        delete port->buff_In;
                        if(lex.size() > 0) {
                            if (lex.ptr()[0] == 0x0A) {
                                switch(lex.ptr()[1]) {
                                    case 0x00: {
                                        if(write
                                                (port->hCom
                                                , lex.ptr()
                                                , lex.size()) != 0 ) {
                                            ;//
                                            } break;
                                        }
                                    case 0x01: {
                                        if(write
                                                (port->hCom
                                                , lex.ptr()
                                                , lex.size()) != 0 ) {
                                            ;//
                                            } break;
                                        }
                                    }
                                }
                            else {
                                if (lex.ptr()[0] == 0xFF) {
                                    lililog.log(LILILog::error, "Recieved incorrect package");
                                    }
                                }
                            }
                        }
                    }
                }
            fd = select.next();
            }
//------------------------------------------------------------------------------
        }
    if(socket) {
        socket->close();
        delete socket;
        }
    if(port) {
        delete port;
        }
    server.close();
    }
//------------------------------------------------------------------------------
// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
