#include <iostream>
#include <lili.h>

class ArmdConfiguration : public LILIConfiguration {
public:
    ArmdConfiguration() {
        DEBUGMode = false;
        }
    int parseToken(LILIString token, LILIString value) {
        if(token == "COMPort") {
            COMPort = value;
            return LILI_OK;
            }
        else if(token == "DEBUGMode") {
            DEBUGMode = atoi(value);
            return LILI_OK;
            }
        else if(token == "DatabasePath") {
            DatabasePath = value;
            return LILI_OK;
            }
        return LILI_ERROR;
        }
    string COMPort;
    string DatabasePath;
    bool DEBUGMode;
    };
