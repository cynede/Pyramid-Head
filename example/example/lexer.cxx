#include "lexer.h"

#define DEBUG
//#define CRC

#ifdef CRC
#define CRC16_POLY      0xA001 //X^16 + X^15 + X^2 + 1
#define CRC16_INIT      0xffff
unsigned short crc16(void* memptr, int len) {
    int j;
    unsigned short crc = CRC16_INIT ;
    while(len--) {
        unsigned char* pchar = (unsigned char*)memptr;  //casting to unsigned char*
        crc = crc ^ pchar[0];                           //XOR computation
        memptr = ++pchar;                               //incrementing the pointer by 1 byte
        for(j = 0; j < 8; j++) {
            if(crc & 1) crc = (unsigned short)((crc >> 1) ^ CRC16_POLY);
            else crc = (unsigned short)(crc >> 1);
            }
        }
    return crc ;
    }
#endif

Lexer::Lexer() {
    op = 0;
    p = 0;
    }

Lexer::~Lexer() { }

LILIBytes Lexer::lex(LILIBytes message) {
    if(message.ptr()[0] == 0x0F) {
#ifdef CRC
        if(crc16(message.ptr(), message.size()) == 0) {
#endif
            if(p == 0)  {
                if(message.ptr()[1] == 0x01)  {
                    unsigned char bytes[] = { 0x0A, 0x00 };
                    return LILIBytes(bytes, 2);
                    }
                else if(message.ptr()[1] == 0x03)  {
                    op = 0;
                    p = 1;
                    unsigned char bytes[] = { 0x0F, 0x0A, 0x01 };
                    return lex(LILIBytes(bytes, 3));
                    }
                else {
                    return LILIBytes();
                    }
                }
            else {
                switch(p) {
                    case 1: {
                        switch(op) {
                            case 0: {
                                if(message.ptr()[1] == 0x0A)  {
                                    op = 0;
                                    p = 0;
                                    unsigned char bytes[] = { 0x0A, 0x01 };
                                    return LILIBytes(bytes, 2);
                                    }
                                }
                            }
                        }
                    case 2: {
                        //2 prms
                        }
                    }
                p--;
                }
#ifdef CRC
            }
#endif
        }
        else {
            unsigned char bytes[] = { 0xFF };
            return LILIBytes(bytes, 1);
            }
    }

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
