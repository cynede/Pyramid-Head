Linux C++ Library
==================

[![Build Status](https://travis-ci.org/Heather/Pyramid-Head.png?branch=master)](https://travis-ci.org/Heather/Pyramid-Head)

<img src="http://fc00.deviantart.net/fs71/f/2012/358/b/3/hirasawa_yui_render_by_maymugilee-d5p0j9n.png"/>

``` cpp
LString path = LString(strcat(Path, "/armd.conf"));
if(configuration.parse(path) != L_OK) {
    strcpy(COMPort, "/dev/ttyS0");
    DEBUGMode = false;
    }
else {
#ifdef WARN
    cout << "COMPort: " << configuration.COMPort << endl;
    cout << "DEBUGMode: " << configuration.DEBUGMode << endl;
    cout << "DatabasePath: " << configuration.DatabasePath << endl;
#endif
    strcpy(COMPort, configuration.COMPort.c_str());
    DEBUGMode = configuration.DEBUGMode;
    }
/// LUA
#ifdef DEBUG
llog.log(LLog::info, "connecting to LUA");
#endif
void *ud = 0;
luaS = lua_newstate(l_alloc, ud);
luaL_openlibs(luaS);
luaL_register(luaS, "dev", Map);
#ifdef DEBUG    
luaL_dofile(luaS, LString(strcat(luaPath, "/lua/example.lua")));
#endif
#ifdef DEBUG
llog.log(LLog::info, "connecting to xdb.db3");
#endif
db = new LDatabase(LString(strcat(dbPath, "/xdb.db3"));
#ifdef DEBUG
llog.log(LLog::info, "connected successful. Starting threads");
#endif
ltime.current();
/// Threadworks
LThread* server_thread;
LThread* twi_thread;
LThread* sync_thread;
LString message1 = LString("main");
/// Daemon threads initialization
server_thread = new LThread(select_server, message1);
sync_thread = new LThread(sync, message1);
twi_thread = new LThread(twi, message1);
/// Opnen SQLite3 db
db->open();
/// Parallel run the daemon threads
server_thread->execute();
```

<img src="http://fc05.deviantart.net/fs70/i/2012/216/c/7/jbf____vocaloid_render1_by_rina_imbers-d59swy6.png"/>
